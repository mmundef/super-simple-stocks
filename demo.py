import datetime
from random import randint, random, choice
from super_simple_stock.stock import StockItem, StockType
from super_simple_stock.trade import Trade, TradeType
from super_simple_stock.calculation import geometric_mean, volume_weighted_stock_price


def seed_price():
    return randint(0, 100) + random()


def seed_created_on():
    start_time_stamp = datetime.datetime.now() - datetime.timedelta(minutes=20)
    end_time_stamp = datetime.datetime.now()
    return randint(int(start_time_stamp.timestamp()), int(end_time_stamp.timestamp()))


def seed_qnt():
    return randint(1, 200)


def seed_trade_type():
    return choice([TradeType.BUY.value, TradeType.SELL.value])


def filter_trades(trades):
    fifteen_minutes_ago = int((datetime.datetime.now() - datetime.timedelta(minutes=15)).timestamp())
    return [item for item in filter(lambda trade: trade.created_on > fifteen_minutes_ago, trades)]


raw_stock_data = [
    {"stock_symbol": 'TEA', "type": StockType.COMMON.value, "last_dividend": 0, "fixed_dividend": 0.0, "par_value": 100,
     'market_price': 4,
     'trade': {
         'created_on': seed_created_on(), 'quantity': seed_qnt(), 'type': seed_trade_type(),
         'price': seed_price(),
     }
     },
    {"stock_symbol": 'POP', "type": StockType.COMMON.value, "last_dividend": 8, "fixed_dividend": 0.0, "par_value": 100,
     'market_price': 12, 'trade': {
        'created_on': seed_created_on(), 'quantity': seed_qnt(),
        'type': seed_trade_type(),
        'price': seed_price(),
    }},
    {"stock_symbol": 'ALE', "type": StockType.COMMON.value, "last_dividend": 23, "fixed_dividend": 0.0, "par_value": 60,
     'market_price': 66, 'trade': {
        'created_on': seed_created_on(), 'quantity': seed_qnt(), 'type': seed_trade_type(),
        'price': seed_price(),
    }},
    {"stock_symbol": 'GIN', "type": StockType.PREFERRED.value, "last_dividend": 8, "fixed_dividend": 2, "par_value": 100,
     'market_price': 90, 'trade': {
        'created_on': seed_created_on(), 'quantity': seed_qnt(), 'type': seed_trade_type(),
        'price': seed_price(),
    }},
    {"stock_symbol": 'JOE', "type": StockType.COMMON.value, "last_dividend": 13, "fixed_dividend": 0.0, "par_value": 250,
     'market_price': 55, 'trade': {
        'created_on': seed_created_on(), 'quantity': seed_qnt(), 'type': seed_trade_type(),
        'price': seed_price(),
    }}]

list_items = []
for item in raw_stock_data[:]:
    trade_data = item.pop('trade')
    stock_item = StockItem(**item)
    trade_data['stock'] = stock_item
    list_items.append(Trade(**trade_data))

fmt_stock = "{stock_symbol} {type} {last_dividend} {fixed_dividend}% " \
            "{par_value} {market_price} {dividend_yield:.2f} {pe_ratio:.2f}"
for item in list_items:
    stock_item = item.stock
    out = fmt_stock.format(stock_symbol=stock_item.stock_symbol,
                           type=stock_item.stock_type,
                           last_dividend=stock_item.last_dividend,
                           fixed_dividend=stock_item.fixed_dividend,
                           par_value=stock_item.par_value,
                           market_price=stock_item.market_price,
                           dividend_yield=stock_item.dividend,
                           pe_ratio=(0.0 if stock_item.dividend == 0 else stock_item.pe_ratio)
                           )
    print(out)

trades = filter_trades(list_items)
print("Volume Weighted Stock Price based on trades in past 15 minutes: %.2f " %
      (0 if not trades else volume_weighted_stock_price(trades)))
print('GBCE All Share Index: %.2f' % geometric_mean(list_items))
