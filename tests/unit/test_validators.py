from unittest import TestCase
from unittest.mock import patch
from super_simple_stock.validators import is_numeric, is_positive_numeric, is_string


class TestValidators(TestCase):

    def test_is_numeric(self):
        self.assertTrue(is_numeric(1))
        self.assertTrue(is_numeric(1.0))
        self.assertTrue(is_numeric(0))

    def test_is_numeric_boolean(self):
        self.assertFalse(is_numeric(True), 'True is not numeric')
        self.assertFalse(is_numeric(False), 'False is not numeric')

    def test_is_numeric_negative(self):
        self.assertFalse(is_numeric('a'))
        self.assertFalse(is_numeric([]))
        self.assertFalse(is_numeric(None))

    def test_is_positive_numeric(self):
        with patch('super_simple_stock.validators.is_numeric') as mocked:
            mocked.return_value = True
            self.assertTrue(is_positive_numeric(1))
            self.assertTrue(is_positive_numeric(0.1))

    def test_is_postive_validator_negative(self):
        with patch('super_simple_stock.validators.is_numeric') as mocked:
            mocked.return_value = True
            self.assertFalse(is_positive_numeric(-1))
            self.assertFalse(is_positive_numeric(0))
            self.assertFalse(is_positive_numeric(0.0))

    def test_is_string(self):
        self.assertTrue(is_string('yes'))
        self.assertFalse(is_string(1))
        self.assertFalse(is_string([]))

