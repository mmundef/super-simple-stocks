from unittest import TestCase
from super_simple_stock.stock import StockItem, StockType


class TestStockItem(TestCase):

    def setUp(self):
        self.data = {
            'stock_symbol': 'test',
            'type': StockType.COMMON.value,
            'last_dividend': 1,
            'fixed_dividend': 1,
            'par_value': 0.1,
            'market_price': 0.1
        }
        # Given I have valid stock item
        self.stock_item = StockItem(**self.data)

    def test_getters(self):
        self.assertEqual(self.stock_item.market_price, self.data['market_price'])
        self.assertEqual(self.stock_item.stock_type, self.data['type'])
        self.assertEqual(self.stock_item.last_dividend, self.data['last_dividend'])
        self.assertEqual(self.stock_item.stock_symbol, self.data['stock_symbol'])
        self.assertEqual(self.stock_item.par_value, self.data['par_value'])
        self.assertEqual(self.stock_item.stock_symbol, self.data['stock_symbol'])
        self.assertEqual(self.stock_item.fixed_dividend, self.data['fixed_dividend'])

    def test_dividends(self):
        dividend = self.stock_item.dividend
        self.assertEqual(self.stock_item.pe_ratio, self.stock_item.market_price / dividend)
        self.assertEqual(self.stock_item.dividend,
                         self.data['last_dividend'] / self.data['market_price'])
        self.assertEqual(self.stock_item.preferred_dividend_yield,
                         self.data['last_dividend'] * self.data['par_value'] / self.data['market_price'])
        common_dividend_value = self.data['last_dividend'] / self.data['market_price']
        self.assertEqual(self.stock_item.common_dividend_yield, common_dividend_value)
        self.assertEqual(self.stock_item.dividend, common_dividend_value)

    def test_setters_validations(self):
        with self.assertRaises(TypeError):
            self.stock_item.stock_symbol = 1
        with self.assertRaises(ValueError):
            self.stock_item.stock_type = 'invaild type'
        with self.assertRaises(ValueError):
            self.stock_item.last_dividend = 'not numeric'
        with self.assertRaises(ValueError):
            self.stock_item.fixed_dividend = 'not numeric'

    def test_division_by_zero_exceptions(self):
        # When I set market_price value to zero
        self.stock_item.market_price = 0

        # Then I expect exceptions about zero-divisions
        with self.assertRaises(ValueError):
            self.assertIsNone(self.stock_item.common_dividend_yield)
        with self.assertRaises(ValueError):
            self.assertIsNone(self.stock_item.preferred_dividend_yield)
