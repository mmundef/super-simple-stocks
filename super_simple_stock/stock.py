from enum import Enum
from .validators import is_numeric, is_string


class StockType(Enum):
    """ Stock Types """
    COMMON = "Common"
    PREFERRED = "Preferred"


class StockItem(object):

    def __init__(self, stock_symbol, type, last_dividend, fixed_dividend, par_value, market_price):
        self.stock_symbol = stock_symbol
        self.stock_type = type
        self.last_dividend = last_dividend
        self.fixed_dividend = fixed_dividend
        self.par_value = par_value
        self.market_price = market_price

    @property
    def stock_symbol(self):
        return self._stock_symbol

    @stock_symbol.setter
    def stock_symbol(self, value):
        if not is_string(value):
            raise TypeError
        self._stock_symbol = value

    @property
    def stock_type(self):
        return self._stock_type

    @stock_type.setter
    def stock_type(self, value):
        if not value in (StockType.COMMON.value, StockType.PREFERRED.value):
            raise ValueError
        self._stock_type = value

    @property
    def last_dividend(self):
        return self._last_dividend

    @last_dividend.setter
    def last_dividend(self, value):
        if not is_numeric(value):
            raise ValueError
        self._last_dividend = value

    @property
    def fixed_dividend(self):
        return self._fixed_dividend

    @fixed_dividend.setter
    def fixed_dividend(self, value):
        if not is_numeric(value):
            raise ValueError
        self._fixed_dividend = value

    @property
    def par_value(self):
        return self._par_value

    @par_value.setter
    def par_value(self, value):
        self._par_value = value

    @property
    def market_price(self):
        return self._market_price

    @market_price.setter
    def market_price(self, value):
        self._market_price = value

    @property
    def common_dividend_yield(self):
        try:
            return float(self.last_dividend / self.market_price)
        except ZeroDivisionError as e:
            raise ValueError from e

    @property
    def preferred_dividend_yield(self):
        try:
            return self.last_dividend * self.par_value / self.market_price
        except ZeroDivisionError as e:
            raise ValueError from e

    @property
    def dividend(self):
        data = {
            StockType.COMMON.value: lambda: self.common_dividend_yield,
            StockType.PREFERRED.value: lambda: self.preferred_dividend_yield
        }
        return data.get(self.stock_type, lambda: None)()

    @property
    def pe_ratio(self):
        return self.market_price / self.dividend
