
def geometric_mean(trades):
    if not isinstance(trades, list):
        raise TypeError

    count_trades = len(trades)
    # Prevent division by zero
    if not count_trades:
        raise ValueError

    return sum([trade.price for trade in trades]) ** (1 / len(trades))


def volume_weighted_stock_price(trades):
    trade_values = 0
    quantity = 0

    for trade in trades:
        trade_values += trade.price * trade.quantity
        quantity += trade.quantity

    return trade_values / quantity

