from enum import Enum
import time
from .stock import StockItem


class TradeType(Enum):
    BUY = 'buy'
    SELL = 'sell'


class Trade(object):

    def __init__(self, stock, quantity, type, price, created_on=None):
        self.stock = stock
        self.quantity = quantity
        self.trade_type = type
        self.price = price
        self.created_on = created_on

    @property
    def quantity(self):
        return self._quantity

    @quantity.setter
    def quantity(self, value):
        self._quantity = value

    @property
    def trade_type(self):
        return self._type

    @trade_type.setter
    def trade_type(self, value):
        if not value in (TradeType.BUY.value, TradeType.SELL.value):
            raise ValueError
        self._type = value

    @property
    def created_on(self):
        return self._created_on

    @created_on.setter
    def created_on(self, value=None):
        if value is None:
            value = int(time.time())
        self._created_on = value

    @property
    def stock(self):
        return self._stock

    @stock.setter
    def stock(self, value):
        if not isinstance(value, StockItem):
            raise TypeError
        self._stock = value
