
def is_numeric(val):
    return isinstance(val, (int, float)) and not isinstance(val, bool)


def is_positive_numeric(val):
    return is_numeric(val) and val > 0.0


def is_string(val):
    return isinstance(val, str)
